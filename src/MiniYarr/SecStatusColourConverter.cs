﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace MiniYarr
{
    public class SecStatusConverter : IValueConverter
    {
        private static readonly SolidColorBrush _Default = new SolidColorBrush(Colors.White);

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var model = value as Character;
            if (model != null)
            {
                Color color;
                var ss = model.SecurityStatus;
                if (ss >= 0.0)
                {
                    color = Colors.Green;
                }
                else
                {
                    var maxSec = 10.0;
                    ss = Math.Max(Math.Abs(ss), maxSec);
                    ss = ss / maxSec;
                    var r = (byte)(int)(255.0 * (ss));
                    var g = (byte)(int)(255.0 * (1.0 - ss));
                    byte b = 40;

                    color = new Color() { A = 255, R = r, G = g, B = b };
                }
                
                return new SolidColorBrush(color);

                
            }

            return _Default;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
