﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniYarr
{
    public class ShipKillInfo
    {
        public int ShipTypeId { get; set; }
        public bool IsIncluded { get; set; }
        public string ShipTypeName { get; set; }
        public string GroupName { get; set; }
        public int Kills { get; set; }
    }
}
