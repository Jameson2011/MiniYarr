﻿namespace MiniYarr
{
    public class Corporation
    {
        public int Id
        {
            get;set;
        }

        public string Name
        {
            get;set;
        }

        public string Ticker
        {
            get;
            set;
        }
        
        public string Faction { get; set; }
    }
}
