﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniYarr
{
    public class GroupPercentage
    {
        public string Name { get; set; }
        public double Value { get; set; }
    }
}
