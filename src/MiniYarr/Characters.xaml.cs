﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace MiniYarr
{
    /// <summary>
    /// Interaction logic for Characters.xaml
    /// </summary>
    public partial class Characters : UserControl
    {
        private ObservableCollection<Character> _Characters = new ObservableCollection<Character>();
        private ICollectionView _CharactersView;
        private CharacterSortKind _CharacterSort = CharacterSortKind.Kills;
        
        public Characters()
        {
            _Characters = new ObservableCollection<Character>(GetSeedCharacters());

            InitializeComponent();

            DataContext = this;

            _CharactersView = CollectionViewSource.GetDefaultView(_Characters);
            SetCharacterSort(_CharacterSort);
        }

       
        
        public CharacterSortKind CharacterSort
        {
            get
            {
                return _CharacterSort;
            }
            set
            {
                _CharacterSort = value;
                SetCharacterSort(value);
            }
        }

        public ObservableCollection<Character> CharacterModels => _Characters;

        public ICollectionView CharacterModelsView => _CharactersView;

        private IList<Character> GetSeedCharacters()
        {
            return new Character[]
            {
#if DEBUG
                new Character() { Name = "Tox Uitra", Id = 95618615, Kills = 2000, Losses = 1337, Dangerous = 0.1, Tags = "Smartbomber, gate camper, don't talk about vegemite", LastFitting = "Smartbomb Proteus", CorpName = "Black Rebel Rifter Club", CorpTicker = "R1FTA", AllianceName = "The Devil's Tattoo", AllianceTicker = "R1DER"},
                new Character() { Name = "BigNTall", Id = 567095312, Kills = 5983, Losses = 12, Dangerous = 0.99, Tags = "Drill sergeant from hell", LastFitting = "Frigates", CorpName = "Black Rebel Rifter Club", CorpTicker = "R1FTA", AllianceName = "The Devil's Tattoo", AllianceTicker = "R1DER"},
                new Character() { Name = "Jameson2011", Id = 90230509, Kills = 1500, Losses = 1500, Dangerous = 0.4, Tags = "Newbie hunter, scrub", LastFitting = "ShitFit Comet; Armour Buffer Vexor", CorpName = "Black Rebel Rifter Club", CorpTicker = "R1FTA", AllianceName = "The Devil's Tattoo", AllianceTicker = "R1DER", },
                new Character() { Name = "Sam Wreck", Id = 94363754, Kills = 1500, Losses = 500, Dangerous = 0.4, Tags = "", LastFitting = "Rifter", CorpName = "Black Rebel Rifter Club", CorpTicker = "R1FTA", AllianceName = "The Devil's Tattoo", AllianceTicker = "R1DER", },
                new Character() { Name = "Sem Skord", Id = 94363754, Kills = 1500, Losses = 200, Dangerous = 0.4, Tags = "El Jefe", LastFitting = "Rifter", CorpName = "Black Rebel Rifter Club", CorpTicker = "R1FTA", AllianceName = "The Devil's Tattoo", AllianceTicker = "R1DER", },
#endif
            };
        }

        private void OnCharacterItemMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                var item = ((FrameworkElement)e.OriginalSource).DataContext as Character;
                if (item != null && item.Id != 0)
                {
                    var zKbUri = $"https://zkillboard.com/character/{item.Id}/";

                    System.Diagnostics.Process.Start(zKbUri);
                }
            }
            catch
            {
                // Swallow the error. Bad practice, but this is just a prototype...
            }
        }


        private void SetCharacterSort(CharacterSortKind sort)
        {
            _CharactersView.SortDescriptions.Clear();
            if (sort == CharacterSortKind.Danger)
            {
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.Dangerous), ListSortDirection.Descending));
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.RelativeDangerous), ListSortDirection.Descending));
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.CorpGroupName), ListSortDirection.Ascending));
            }
            else if (sort == CharacterSortKind.RelativeDanger)
            {
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.RelativeDangerous), ListSortDirection.Descending));
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.Dangerous), ListSortDirection.Descending));
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.CorpGroupName), ListSortDirection.Ascending));
            }
            else if (sort == CharacterSortKind.Kills)
            {
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.Kills), ListSortDirection.Descending));
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.Losses), ListSortDirection.Ascending));
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.CorpGroupName), ListSortDirection.Ascending));
            }
            else if (sort == CharacterSortKind.Gangs)
            {
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.Gangs), ListSortDirection.Descending));
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.RelativeDangerous), ListSortDirection.Descending));
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.Dangerous), ListSortDirection.Descending));
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.CorpGroupName), ListSortDirection.Ascending));
            }
            else
            {
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.CorpGroupName), ListSortDirection.Ascending));
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.RelativeDangerous), ListSortDirection.Descending));
                _CharactersView.SortDescriptions.Add(new SortDescription(nameof(Character.Dangerous), ListSortDirection.Descending));
                
            }
        }
    }
}
