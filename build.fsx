#r @"packages/FAKE/tools/FakeLib.dll"
#r @"packages/FAKE.IO.FileSystem/lib/netstandard2.0/Fake.IO.FileSystem.dll"

open Fake.Core
open Fake.Core.TargetOperators
open Fake.IO.Globbing.Operators
open Fake.IO
open Fake.DotNet


// Definitions
let buildDir = "build/"
let zipDir = "artifacts"
let buildFiles = "build/**/*.*"
let buildFilesExclude = "build/**/*.pdb"
let appProjects = "src/MiniYarr.sln"
let zipFile = zipDir + "/MiniYarr.zip"

// Targets
Target.create "ScrubArtifacts" (fun _ -> Shell.cleanDirs [ buildDir; zipDir; ] )

Target.create "BuildApp" (fun _ -> 
                            appProjects
                            |> MSBuild.build (fun opts ->
                                                            { opts with
                                                                RestorePackagesFlag = false
                                                                Targets = ["Rebuild"]
                                                                Verbosity = Some MSBuildVerbosity.Minimal
                                                                Properties =
                                                                  [ "VisualStudioVersion", "15.0"
                                                                    "Configuration", "Release"
                                                                  ]
                                                            })
                            )
Target.create "ZipApp" (fun _ -> !!buildFiles --buildFilesExclude
                                    |> Zip.createZip zipDir zipFile "" 9 true  )

Target.create "All" (fun _ -> Trace.trace "Built" )

// Dependencies

"ScrubArtifacts" 
==> "BuildApp"
==> "ZipApp"
==> "All"

Target.runOrDefault "All"
