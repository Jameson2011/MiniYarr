﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MiniYarr
{
    public class Character : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _Name;
        private int _Kills;
        private int _Losses;
        private double _Dangerous;
        private double _RelativeDangerous;
        private long _IskLost;
        private long _IskDestroyed;
        private double _Gangs;
        private string _Tags;
        private TimeSpan _StatsAge;
        private IList<ShipKillInfo> _KillInfos = new List<ShipKillInfo>();
        private ShipKillAnalysis _KillAnalysis = new ShipKillAnalysis();
        private IList<ShipKillInfo> _RecentKillInfos = new List<ShipKillInfo>();
        private ShipKillAnalysis _RecentKillAnalysis = new ShipKillAnalysis();

        public int Id
        {
            get; set;
        }

        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (_Name != value)
                {
                    _Name = value;
                    RaisePropertyChanged(nameof(Name));
                }
            }
        }

        public TimeSpan StatsAge
        {
            get { return _StatsAge; }
            set
            {
                if (_StatsAge != value)
                {
                    _StatsAge = value;
                    RaisePropertyChanged(nameof(StatsAge));
                }
            }
        }

        public string StatsMessage
        {
            get
            {
                if (_StatsAge > TimeSpan.FromHours(1))
                {
                    return $"Old stats! Last update was {_StatsAge}";
                }
                else if (_StatsAge > TimeSpan.FromMinutes(30))
                {
                    return "Getting old. Last update was {_StatsAge}";
                }
                return "";
            }
        }

        public DateTime Birthday
        {
            get; set;
        }

        public double SecurityStatus
        {
            get; set;
        }

        public string CorpTicker
        {
            get; set;
        }

        public int CorpId { get; set; }
        public int AllianceId { get; set; }
        public string CorpName
        {
            get; set;
        }

        public string AllianceName
        {
            get; set;
        }

        public string CorpGroupName
        {
            get
            {
                return AllianceName ?? CorpName;
            }
        }

        public string AllianceTicker
        {
            get; set;
        }

        public string Faction
        {
            get; set;
        }

        public string Tags
        {
            get
            {
                return _Tags;
            }
            set
            {
                if (_Tags != value)
                {
                    _Tags = value;
                    RaisePropertyChanged(nameof(Tags));
                }
            }
        }

        public string LastFitting
        {
            get; set;
        }

        public int Kills
        {
            get
            {
                return _Kills;
            }
            set
            {
                if (_Kills != value)
                {
                    _Kills = value;
                    RaisePropertyChanged(nameof(Kills));
                }
            }
        }

        public int Losses
        {
            get
            {
                return _Losses;
            }
            set
            {
                if (_Losses != value)
                {
                    _Losses = value;
                    RaisePropertyChanged(nameof(Losses));
                }
            }
        }


        public long IskLost
        {
            get
            {
                return _IskLost;
            }
            set
            {
                if (_IskLost != value)
                {
                    _IskLost = value;
                    RaisePropertyChanged(nameof(IskLost));
                }
            }
        }

        public long IskDestroyed
        {
            get
            {
                return _IskDestroyed;
            }
            set
            {
                if (_IskDestroyed != value)
                {
                    _IskDestroyed = value;
                    RaisePropertyChanged(nameof(IskDestroyed));
                }
            }
        }

        public double Dangerous
        {
            get
            {
                return _Dangerous;
            }
            set
            {
                if (_Dangerous != value)
                {
                    _Dangerous = value;
                    RaisePropertyChanged(nameof(Dangerous));
                }
            }
        }

        public double RelativeDangerous
        {
            get
            {
                return _RelativeDangerous;
            }
            set
            {
                if (_RelativeDangerous != value)
                {
                    _RelativeDangerous = value;
                    RaisePropertyChanged(nameof(RelativeDangerous));
                }
            }
        }

        public double Gangs
        {
            get
            {
                return _Gangs;
            }
            set
            {
                if (_Gangs != value)
                {
                    _Gangs = value;
                    RaisePropertyChanged(nameof(Gangs));
                }
            }
        }

        public IList<ShipKillInfo> KillInfos
        {
            get { return _KillInfos; }

            set
            {
                _KillInfos = value ?? new List<ShipKillInfo>();
            }
        }

        public ShipKillAnalysis KillAnalysis
        {
            get
            {
                return _KillAnalysis;
            }
            set
            {
                _KillAnalysis = value;
            }
        }


        public IList<ShipKillInfo> RecentKillInfos
        {
            get { return _RecentKillInfos; }

            set
            {
                _RecentKillInfos = value ?? new List<ShipKillInfo>();
            }
        }

        public ShipKillAnalysis RecentKillAnalysis
        {
            get
            {
                return _RecentKillAnalysis;
            }
            set
            {
                _RecentKillAnalysis = value;
            }
        }


        private void RaisePropertyChanged(string name)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
