﻿using System;

namespace MiniYarr
{
    public interface IHwndSource
    {

        IntPtr Handle { get; }
        ClipboardPipe.MessageReceived this[int msgType] { get; set; }
    }
}
