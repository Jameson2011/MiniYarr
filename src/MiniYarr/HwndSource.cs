﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MiniYarr
{
    public sealed class HwndSource : NativeWindow, IHwndSource
    {
        private readonly Dictionary<int, ClipboardPipe.MessageReceived> _messageDelegatesByMessageType =
            new Dictionary<int, ClipboardPipe.MessageReceived>();

        public HwndSource()
        {
            var p = new CreateParams();
            CreateHandle(p);

            
        }
        
        public ClipboardPipe.MessageReceived this[int msgType]
        {
            get
            {
                ClipboardPipe.MessageReceived myDelegate;
                return _messageDelegatesByMessageType.TryGetValue(msgType, out myDelegate) ? myDelegate : null;
            }

            set
            {
                if (_messageDelegatesByMessageType.ContainsKey(msgType))
                {
                    _messageDelegatesByMessageType[msgType] += value;
                    return;
                }
                _messageDelegatesByMessageType[msgType] = value;
            }
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            var messageDelegate = this[m.Msg];
            if (messageDelegate != null)
            {
                messageDelegate(m);
            }
        }



        ~HwndSource()
        {
            DestroyHandle();
        }
    }
}
