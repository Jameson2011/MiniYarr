﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Net.Http;
using System.Globalization;

namespace MiniYarr
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ClipboardPipe _ClipboardPipe;
        private DataProvider _DataProvider;
        private HttpClient _Client;

        public MainWindow()
        {
            InitializeComponent();

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag);

            this.Topmost = true;
            
            this.Top = 0;
            var sw = SystemParameters.PrimaryScreenWidth;
            var w = this.Width;
            this.Left = (sw / 2) - (w / 2);
            
            SourceInitialized += MainWindow_SourceInitialized;

            
            var handler = new HttpClientHandler()
            {
                AutomaticDecompression = System.Net.DecompressionMethods.GZip,
            };

            _Client = new HttpClient(handler);
            _Client.DefaultRequestHeaders.UserAgent.ParseAdd("MiniYarr");
            _Client.DefaultRequestHeaders.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));

            _DataProvider = new DataProvider(_Client);

            this.Deactivated += MainWindow_Deactivated;
            this.Activated += MainWindow_Activated;

            DataContext = this;

        }

        private void MainWindow_Activated(object sender, EventArgs e)
        {
            this.Characters.Opacity = 1;
        }

        private void MainWindow_Deactivated(object sender, EventArgs e)
        {
            this.Characters.Opacity = 0.5;
        }

        private void Paste_CanExecuteChanged(object sender, EventArgs e)
        {
            if(Clipboard.ContainsText())
            {
                var asciiData = Clipboard.GetText();
                if(asciiData != null)
                {

                }
            }
        }

        private void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            StartClipboardPipe();
        }

        private void StartClipboardPipe()
        {
            
            _ClipboardPipe = new ClipboardPipe(new HwndSource());
            
            _ClipboardPipe.NewClipboardAsciiData += Clipboard_NewClipboardAsciiData;
        }

        private async void Clipboard_NewClipboardAsciiData(object sender, string text)
        {
            
            var names = text.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                            .Select(s => s.Trim())
                            .Distinct()
                            .ToList();
            var filter = new CharacterListFilter();
            names = filter.FilterNames(names).ToList();
            if (names.Count > 0 )
            {
                try
                {
                    this.Characters.CharacterModels.Clear();

                    var result = new List<Character>();
                    var chars = await _DataProvider.GetCharacterIds(names);
                    var stats = new StatsInfo();

                    foreach (var c in chars)
                    {
                        var c2 = await _DataProvider.GetCharacterAsync(c);
                        if (c2 != null)
                        {
                            var character = await _DataProvider.GetCharacterStats(c2);
                            if (character != null)
                            {
                                result.Add(character);
                                stats.Kills += character.Kills;
                                stats.Losses += character.Losses;
                                stats.IskKilled += character.IskDestroyed;
                                stats.IskLost += character.IskLost;

                                _DataProvider.CalculateRelativeStats(stats, result);

                                this.Characters.CharacterModels.Add(character);
                            }
                        }
                    }


                }
                catch (Exception)
                {
                    // 
                }
            }

        }

        public CharacterSortKind CharacterSort
        {
            get
            {
                return Characters.CharacterSort;
            }
            set
            {
                Characters.CharacterSort = value;
            }
        }

        private void TitleBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
