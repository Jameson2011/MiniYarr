﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace MiniYarr
{
    public class DangerousColourConverter : IValueConverter
    {
        private static readonly SolidColorBrush _Default = new SolidColorBrush(Colors.White);

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var model = value as Character;
            if (model != null)
            {
                var r = (byte)(int)(255.0 * (model.Dangerous));
                var g = (byte)(int)(255.0 * (1.0 - model.Dangerous));
                byte b = 0;

                var color = new Color() {A=255, G = g, B = b} ;
                color.R = (byte)r;

                return new SolidColorBrush(color);

                
            }

            return _Default;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
