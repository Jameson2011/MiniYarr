﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniYarr
{
    public class RetryableException : Exception
    {
        public RetryableException(string message) : base(message)
        {

        }
    }
}
