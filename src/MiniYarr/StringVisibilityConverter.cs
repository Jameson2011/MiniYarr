﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MiniYarr
{
    public class StringVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = value as string;
            var flip = true;
            if(parameter != null)
            {
                if(parameter is string)
                {
                    bool.TryParse(parameter.ToString(), out flip);
                }
            }
            return !String.IsNullOrEmpty(str) == flip
                ? Visibility.Visible
                : Visibility.Collapsed;            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
