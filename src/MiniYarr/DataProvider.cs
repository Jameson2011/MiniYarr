﻿using IronSde;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MiniYarr
{
    public class DataProvider
    {
        private readonly HttpClient _Client;

        private readonly Lazy<Dictionary<int, string>> _Factions;

        public DataProvider(HttpClient client)
        {
            _Client = client;
            _Factions = new Lazy<Dictionary<int, string>>(() => GetFactionNames(client));
        }


        public async Task<Character> GetCharacterAsync(Character character)
        {
            for (var x = 0; x < 5; x++)
            {
                try
                {
                    return await GetCharacterInner(character);
                }
                catch (RetryableException)
                {
                    if (x >= 5)
                        return null;
                }
                catch (Exception)
                {
                }
            }

            return null;
        }

        private async Task<Character> GetCharacterInner(Character character)
        {
            var uri = $"https://esi.evetech.net/latest/characters/{character.Id}/?datasource=tranquility&language=en-us";

            using (var resp = await _Client.GetAsync(uri))
            {

                if (resp.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    if (resp.StatusCode == System.Net.HttpStatusCode.GatewayTimeout)
                    {
                        throw new RetryableException($"HTTP status {resp.StatusCode} returned from ESI");
                    }
                    throw new InvalidOperationException($"HTTP status {resp.StatusCode} returned from ESI");
                }

                var respJson = await GetContent(resp);
                var r = JsonConvert.DeserializeObject(respJson);
                var xs = r as Newtonsoft.Json.Linq.JObject;
                if (xs != null)
                {
                    var bday = xs["birthday"];
                    if (bday != null)
                    {
                        character.Birthday = (DateTime)bday;
                    }
                    var secStatus = xs["security_status"];
                    if (secStatus != null)
                    {
                        character.SecurityStatus = (double)secStatus;
                    }
                    var corpId = xs["corporation_id"];
                    if (corpId != null)
                    {
                        character.CorpId = (int)corpId;
                        var corp = await GetCorporation((int)corpId);
                        if (corp != null)
                        {
                            character.CorpName = corp.Name;
                            character.CorpTicker = corp.Ticker;
                            character.Faction = corp.Faction;
                        }
                    }
                    var allianceId = xs["alliance_id"];
                    if (allianceId != null)
                    {
                        var alliance = await GetAlliance((int)allianceId);
                        if (alliance != null)
                        {
                            character.AllianceId = (int)allianceId;
                            character.AllianceName = alliance.Name;
                            character.AllianceTicker = alliance.Ticker;
                            character.Faction = alliance.Faction;
                        }
                    }

                }

                return character;
            }
        }
        public async Task<Corporation> GetCorporation(int id)
        {
            
            for (var x = 0; x < 5; x++)
            {
                try
                {
                    return await GetCorporationEntity(id, "corporations");
                }
                catch (RetryableException)
                {
                    if (x >= 5) return null;
                }
                catch (Exception)
                {
                    
                }
            }
            return null;
        }

        public async Task<Corporation> GetAlliance(int id)
        {
            for (var x = 0; x < 5; x++)
            {
                try
                {
                    return await GetCorporationEntity(id, "alliances");
                }
                catch (RetryableException)
                {
                    if (x >= 5) return null;
                }
                catch (Exception)
                {
                    
                }
            }
            return null;
        }


        public async Task<Corporation> GetCorporationEntity(int id, string type)
        {
            var version = type == "alliances" ? "v3" : "v4";

            var uri = $"https://esi.evetech.net/{version}/{type}/{id}/?datasource=tranquility&language=en-us";

            using (var resp = await _Client.GetAsync(uri))
            {

                if (resp.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    if (resp.StatusCode == System.Net.HttpStatusCode.GatewayTimeout)
                    {
                        throw new RetryableException($"HTTP status {resp.StatusCode} returned from ESI");
                    }
                    throw new InvalidOperationException($"HTTP status {resp.StatusCode} returned from ESI");
                }

                var respJson = await GetContent(resp);
                var r = JsonConvert.DeserializeObject(respJson);
                var xs = r as Newtonsoft.Json.Linq.JObject;
                if (xs != null)
                {
                    var result = new Corporation()
                    {
                        Id = id
                    };

                    var n = xs["name"];
                    if (n != null)
                    {
                        result.Name = (string)n;
                    }
                    var t = xs["ticker"];
                    if (t != null)
                    {
                        result.Ticker = (string)t;
                    }

                    var a = xs["alliance_id"];
                    if (a != null)
                    {

                    }

                    var f = xs["faction_id"];
                    if (f != null)
                    {
                        var fid = (int)f;
                        if (_Factions.Value.TryGetValue(fid, out string name))
                        {
                            result.Faction = name;
                        }
                        else
                        {
                            result.Faction = fid.ToString();
                        }
                    }
                    return result;
                }
            }
            return null;
        }



        public async Task<IList<Character>> GetCharacterIds(IEnumerable<string> names)
        {
            var uri = "https://esi.evetech.net/latest/universe/ids/?datasource=tranquility&language=en-us";


            var json = JsonConvert.SerializeObject(names.ToList());

            var payload = new StringContent(json, Encoding.UTF8, "application/json");


            var resp = await _Client.PostAsync(uri, payload);

            if (resp.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new InvalidOperationException($"HTTP status {resp.StatusCode} returned from ESI");
            }

            var respJson = await GetContent(resp);

            var r = JsonConvert.DeserializeObject(respJson);
            var xs = r as Newtonsoft.Json.Linq.JObject;
            if (xs != null)
            {
                var cs = xs["characters"];
                if (cs != null)
                {
                    var ids = cs.Select(t => new Character()
                    {
                        Id = (int)t["id"],
                        Name = t["name"].ToString()
                    }).ToList();

                    return ids;
                }
            }

            return new List<Character>();

        }

        public async Task<Character> GetCharacterStats(Character character)
        {
            for (var x = 0; x < 5; x++)
            {
                try
                {
                    return await GetCharacterStatsInner(character);
                }
                catch (RetryableException)
                {
                    if (x >= 5) return null;
                }
                catch (Exception)
                {
                    
                }
            }

            return null;
        }

        private async Task<Character> GetCharacterStatsInner(Character character)
        {

            var uri = $"https://zkillboard.com/api/stats/characterID/{character.Id}/";

            using (var resp = await _Client.GetAsync(uri))
            {

                var respJson = await GetContent(resp);


                if (resp.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    if (resp.StatusCode == System.Net.HttpStatusCode.GatewayTimeout)
                    {
                        throw new RetryableException($"HTTP status {resp.StatusCode} returned from ESI");
                    }

                    throw new InvalidOperationException($"HTTP status {resp.StatusCode} returned from ESI");

                }

                var j = JsonConvert.DeserializeObject(respJson);
                var xs = j as Newtonsoft.Json.Linq.JObject;
                if (xs != null)
                {
                    var x = xs["shipsLost"];
                    if (x != null)
                    {
                        character.Losses = (int)x;
                    }
                    x = xs["shipsDestroyed"];
                    if (x != null)
                    {
                        character.Kills = (int)x;
                    }
                    x = xs["iskDestroyed"];
                    if (x != null)
                    {
                        character.IskDestroyed = (long)x;
                    }
                    x = xs["iskLost"];
                    if (x != null)
                    {
                        character.IskLost = (long)x;
                    }


                    x = xs["gangRatio"];
                    if (x != null)
                    {
                        character.Gangs = ((int)x) / 100.0;
                    }

                    var danger = CalculateDanger(character);
                    character.Dangerous = danger;


                    /*
                    x = xs["dangerRatio"];
                    if(x != null)
                    {
                        character.Dangerous = ((int)x) / 100.0;
                    }
                    */
                    var allTime = xs["topAllTime"];
                    if (allTime != null)
                    {
                        var allTimeShips = allTime.Where(jt => jt["type"].ToString() == "ship").FirstOrDefault();
                        if (allTimeShips != null)
                        {
                            var kills = allTimeShips.SelectMany(jt2 => jt2).OfType<JArray>().SelectMany(ys => ys)
                                .Select(jt2 => GetShipKillData((int)jt2["shipTypeID"], (int)jt2["kills"]))
                                .Where(i => i != null && i.IsIncluded)
                                .OrderByDescending(t => t.Kills)
                                .ToList();

                            character.KillInfos = kills;
                            character.KillAnalysis = GetKillAnalysis(kills);
                        }
                    }

                    var topLists = xs["topLists"];
                    if (topLists != null)
                    {
                        var topShips = topLists.Where(jt => jt["type"].ToString() == "shipType").FirstOrDefault();
                        if (topShips != null)
                        {
                            var ships = topShips.SelectMany(jt2 => jt2).OfType<JArray>().SelectMany(ys => ys)
                                .Select(jt2 => GetShipKillData((int)jt2["shipTypeID"], (int)jt2["kills"]))
                                .Where(i => i != null && i.IsIncluded)
                                .OrderByDescending(ski => ski.Kills)
                                .ToList();

                            character.RecentKillInfos = ships;
                            character.RecentKillAnalysis = GetKillAnalysis(ships);
                        }
                    }

                    character.Tags = GetTags(character);
                }



                return character;
            }
        }

        public string GetTags(Character character)
        {
            return $"Gangs: {character.Gangs:P} Dangerous: {character.Dangerous:P}";
        }

        public IList<Character> CalculateRelativeStats(StatsInfo accum, IList<Character> characters)
        {
            foreach (var character in characters)
            {
                var losses = (character.Losses * character.IskLost) / (accum.Losses * accum.IskLost);
                var kills = (character.Kills * character.IskDestroyed) / (accum.Kills * accum.IskKilled);

                var factor = losses + kills;

                character.RelativeDangerous = factor != 0.0 ? kills / factor : 0;

                character.Tags = GetTags(character);
            }


            return characters;
        }

        private ShipKillInfo GetShipKillData(int shipId, int kills)
        {
            var ship = ItemTypes.GetItemType(shipId);
            if (ship != null)
            {

                return new ShipKillInfo()
                {
                    ShipTypeId = shipId,
                    ShipTypeName = ship.Value.name,
                    IsIncluded = ship.Value.group.key != ItemTypeGroups.Capsule &&
                                    ship.Value.group.key != ItemTypeGroups.Corvette,
                    GroupName = ship.Value.group.name,
                    Kills = kills,
                };
            }
            return new ShipKillInfo()
            {
                ShipTypeId = shipId,
                ShipTypeName = $"Unknown type {shipId}",
                IsIncluded = true,
                GroupName = "Unknown group",
                Kills = kills,
            }; ;
        }

        private ShipKillAnalysis GetKillAnalysis(IList<ShipKillInfo> kills)
        {
            var result = new ShipKillAnalysis();

            var totalKills = kills.Sum(k => k.Kills);

            var shipStats = kills.GroupBy(k => k.ShipTypeName, k => k.Kills)
                .Select(grp => (grp.Key.ToString(), grp.Sum()))
                .OrderByDescending(t => t.Item2)
                .Select(t => new GroupPercentage()
                {
                    Name = t.Item1,
                    Value = ((double)t.Item2 / totalKills) * 100.0,
                }).Where(p => p.Value >= 1)
                .Take(10);

            var groupStats = kills.GroupBy(k => k.GroupName, k => k.Kills)
                .Select(grp => (grp.Key.ToString(), grp.Sum()))
                .OrderByDescending(t => t.Item2)
                .Select(t => new GroupPercentage()
                {
                    Name = t.Item1,
                    Value = ((double)t.Item2 / totalKills) * 100.0,
                }).Where(p => p.Value >= 1)
                .Take(10);

            result.ShipKillStats = shipStats.ToList();
            result.ShipGroupKillStats = groupStats.ToList();

            return result;

        }

        private double CalculateDanger(Character character)
        {
            // TODO: still woeful wrt to low stats characters

            var destroyed = (double)character.IskDestroyed * (double)character.Kills;
            var lost = (double)character.IskLost * (double)character.Losses;
            var factor = (lost + destroyed);

            return factor != 0.0 ? destroyed / factor : 0;
        }

        private Dictionary<int, string> GetFactionNames(HttpClient client)
        {
            var result = new Dictionary<int, string>();
            var uri = "https://esi.evetech.net/v2/universe/factions/?datasource=tranquility&language=en-us";

            var resp = client.GetAsync(uri).Result;

            if (resp.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new InvalidOperationException($"HTTP status {resp.StatusCode} returned from ESI");
            }

            var respJson = GetContent(resp).Result;
            var r = JsonConvert.DeserializeObject(respJson);
            var xs = r as Newtonsoft.Json.Linq.JArray;
            if (xs != null)
            {
                foreach (var x in xs)
                {
                    var id = x["faction_id"];
                    if (id != null)
                    {
                        var fid = (int)id;
                        var name = x["name"];
                        result[fid] = (string)name;
                    }
                }
            }

            return result;
        }

        private async Task<string> GetContent(HttpResponseMessage resp)
        {
            return await resp.Content.ReadAsStringAsync();
        }

        private void LogError(Exception ex)
        {
            Console.Error.WriteLine(ex.Message);
        }
    }
}
