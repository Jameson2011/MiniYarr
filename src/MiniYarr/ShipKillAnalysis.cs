﻿using System.Collections.Generic;

namespace MiniYarr
{
    public class ShipKillAnalysis
    {
        public ShipKillAnalysis()
        {
            ShipKillStats = new List<GroupPercentage>();
            ShipGroupKillStats = new List<GroupPercentage>();
        }

        public IList<GroupPercentage> ShipKillStats { get; set; }

        public IList<GroupPercentage> ShipGroupKillStats { get; set; }
    }
}
