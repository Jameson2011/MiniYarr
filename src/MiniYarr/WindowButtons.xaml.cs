﻿using System.Windows;
using System.Windows.Controls;

namespace MiniYarr
{
    /// <summary>
    /// Interaction logic for WindowButtons.xaml
    /// </summary>
    public partial class WindowButtons : UserControl
    {

        private const int normalHeight = 304;
        private const int minimisedHeight = 32;

        public WindowButtons()
        {
            InitializeComponent();
        }


        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();

        }

        private void Resize_Click(object sender, RoutedEventArgs e)
        {
            ResizeWindow(normalHeight, minimisedHeight);
        }

        private void Expand_Click(object sender, RoutedEventArgs e)
        {
            ResizeWindow(SystemParameters.MaximizedPrimaryScreenHeight, minimisedHeight);

        }

        private void ResizeWindow(double max, double min)
        {
            // HACK: as filthy as they come

            if (Application.Current.MainWindow.Height >= max)
            {
                Application.Current.MainWindow.Height = min;
            }
            else
            {
                Application.Current.MainWindow.Height = max;
            }
        }
    }
}
