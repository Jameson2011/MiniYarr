﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniYarr
{
    public class StatsInfo
    {
        public double Kills { get; set; }
        public double Losses { get; set; }
        public double IskKilled { get; set; }
        public double IskLost { get; set; }
    }
}
