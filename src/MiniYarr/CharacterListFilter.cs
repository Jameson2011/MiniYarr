﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace MiniYarr
{
    public class CharacterListFilter
    {
        private static readonly char[] _whitespaceDelimiters = new[] { ' ' };
        private static readonly XmlReaderSettings _rdrSettings = new XmlReaderSettings
        {
            ConformanceLevel = ConformanceLevel.Fragment,
            IgnoreWhitespace = true
        };

        public IList<string> FilterNames(IList<string> lines)
        {
            var names = FilterNamesInner(lines).ToList();
            if(names.Count == lines.Count)
            {
                return names;
            }
            return Array.Empty<string>();
        }

        public string NameLinkToName(string text)
        {
            if (text == null || !text.StartsWith("<font "))
            {
                return null;
            }

            var xml = ReadXml(text).OfType<XElement>();

            var innerNodes = xml.Where(xe => !string.IsNullOrWhiteSpace(xe.Value))
                                .Select(xe => xe.FirstNode as XElement).Where(x => x != null && x.Name.LocalName == "a")
                                .Take(1).ToList();
            if (innerNodes.Count > 0)
            {
                var a = innerNodes[0].FirstAttribute?.Value;

                if (a != null && a.StartsWith("showinfo:"))
                {
                    return innerNodes[0].Value;
                }
            }

            return null;
        }
        
        private IEnumerable<string> FilterNamesInner(IList<string> lines)
        {
            foreach (var l in lines)
            {
                if (IsPossibleCharacter(l))
                {
                    yield return l;
                }
                else
                {
                    var linkedName = NameLinkToName(l);
                    if (linkedName != null)
                    {
                        yield return linkedName;
                    }
                }

            }

        }


        private bool IsPossibleCharacter(string line)
        {
            // https://community.eveonline.com/support/policies/naming-policy-en/
            return CharsOk(line) && NameLengthOK(line) && NameComponentsOk(line);
        }


        private bool CharsOk(string text)
        {
            foreach (var c in text)
            {
                if (!Char.IsLetterOrDigit(c) && c != ' ' && c != '-' && c != '\'')
                {
                    return false;
                }
            }
            return true;
        }

        private bool NameLengthOK(string text)
        {
            return text.Length >= 3 && text.Length <= 37;
        }

        private bool NameComponentsOk(string text)
        {
            var idx = text.LastIndexOf(' ');
            if (idx >= 0)
            {
                var first = text.Substring(0, idx).Trim();
                var family = text.Substring(idx).Trim();

                return first.Length <= 24 && family.Length <= 24;
            }
            return true;
        }

        private IList<XNode> ReadXml(string text)
        {
            try
            {
                return ReadXmlInner(text).ToList();
            }
            catch (XmlException)
            {
                return new List<XNode>();
            }
        }

        private IEnumerable<XNode> ReadXmlInner(string text)
        {
            using (var rdr = new StringReader(text))
            {
                using (var xmlReader = XmlReader.Create(rdr, _rdrSettings))
                {
                    xmlReader.MoveToContent();
                    while (xmlReader.ReadState != ReadState.EndOfFile)
                    {
                        yield return XNode.ReadFrom(xmlReader);
                    }
                }
            }
        }
    }

}
