﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace MiniYarr
{

    public class ClipboardPipe : IDisposable
    {
        public delegate void MessageReceived(Message msg);
        public event Action<object, string> NewClipboardAsciiData;

        private IntPtr _nextClipboardViewer;
        private IHwndSource _window;

        [DllImport("User32.dll")]
        protected static extern int SetClipboardViewer(int hWndNewViewer);


        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool ChangeClipboardChain(IntPtr hWndRemove, IntPtr hWndNewNext);


        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);

        public ClipboardPipe(IHwndSource window)
        {
            _window = window;
            SetHook(window);
        }

        public void Dispose()
        {
            ChangeClipboardChain(_window.Handle, _nextClipboardViewer);
        }

        ~ClipboardPipe()
        {
            ChangeClipboardChain(_window.Handle, _nextClipboardViewer);
        }

        private void SetHook(IHwndSource window)
        {
            
            const int WM_DRAWCLIPBOARD = 0x308;
            const int WM_CHANGECBCHAIN = 0x030D;

            window[WM_DRAWCLIPBOARD] += OnDrawClipboardMessageReceived;
            window[WM_CHANGECBCHAIN] += OnChangeCbChainMessageReceived;

            _nextClipboardViewer = (IntPtr)SetClipboardViewer(window.Handle.ToInt32());
        }


        private void OnChangeCbChainMessageReceived(Message msg)
        {
            if (msg.WParam == _nextClipboardViewer)
            {
                _nextClipboardViewer = msg.LParam;
            }
            else
            {
                SendMessage(_nextClipboardViewer, msg.Msg, msg.WParam, msg.LParam);
            }
        }

        private void OnDrawClipboardMessageReceived(Message msg)
        {
            if (Clipboard.ContainsText())
            {
                var asciiData = Clipboard.GetText();
                OnNewClipboardAsciiData(asciiData);
            }
            SendMessage(_nextClipboardViewer, msg.Msg, msg.WParam, msg.LParam);
        }

        private void OnNewClipboardAsciiData(string asciiData)
        {
            var handler = NewClipboardAsciiData;
            if (handler != null) handler(this, asciiData);
        }

    }
}
